'''
@author: michal
'''

import os,shutil
import numpy as np
import stl

class JobBase(object):
    '''
    Base class for geometry provided by user
    '''

    # private
    _baseDir = None
    _workDir = None
    _caseDir = None
    _case = None
    _geometryFileCopyTo = "./geometry.stl"
    _geometryBB = None
    _caseFullPath = None
    _solver = None
    _verbose = False
    # public
    caseDirExists = False

    def __init__(self,srcBaseDir,workDir,case,solver,verbose=False):
        '''
        Constructor
        '''
        # private
        self._srcBaseDir = srcBaseDir
        self._workDir = workDir
        self._caseDir = os.path.join(workDir,case)
        self._case = case
        self._solver = solver
        self._caseFullPath = os.path.join(self._workDir,self._case)
        self._verbose = verbose
        # public
        self.caseDirExists = os.path.exists(self._caseFullPath)

    def loadGeometryFile(self, fileName):
        '''
        Put geometry file to case directory
        Calculate geometry bounding box
        '''
        try:
            shutil.copy(fileName,os.path.join(self._caseDir,self._geometryFileCopyTo))
        except FileNotFoundError:
            print("Geometry file not found")
            raise

        # Using an existing closed stl file:
        mesh = stl.mesh.Mesh.from_file(fileName)
        self._geometryBB = self._getGeometryBoundingBox(mesh)

    def _getGeometryBoundingBox(self,mesh):
        '''
        Calculate geometry bounding box
        '''
        minx = maxx = miny = maxy = minz = maxz = None
        for p in mesh.points:
            # p contains (x, y, z)
            if minx is None:
                minx = p[stl.Dimension.X]
                maxx = p[stl.Dimension.X]
                miny = p[stl.Dimension.Y]
                maxy = p[stl.Dimension.Y]
                minz = p[stl.Dimension.Z]
                maxz = p[stl.Dimension.Z]
            else:
                maxx = max(p[stl.Dimension.X], maxx)
                minx = min(p[stl.Dimension.X], minx)
                maxy = max(p[stl.Dimension.Y], maxy)
                miny = min(p[stl.Dimension.Y], miny)
                maxz = max(p[stl.Dimension.Z], maxz)
                minz = min(p[stl.Dimension.Z], minz)
        return [[minx, miny, minz], [maxx, maxy, maxz]]

    def preprocessGeometry(self):
        pass

    def preprocessMesh(self):
        pass

    def setupCase(self,param):
        pass

    def checkMesh(self):
        pass

    def refineMesh(self,param):
        pass

    def createCase(self):
        pass

    def runSolver(self):
        pass
