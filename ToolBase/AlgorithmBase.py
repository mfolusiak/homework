'''
@author: michal
'''

class AlgorithmBase(object):
    '''
    Base class for the execution algorithm
    '''

    def __init__(self,job):
        self.job = job

    def run(self):
        self.job.preprocessGeometry()
        self.job.preprocessMesh()
        self.job.runSolver()