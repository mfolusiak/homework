'''
@author: michal
'''
from enum import Enum
class allowedSolverStatus(Enum):
    converged = 1
    stalled = 2
    failed = 3
    maxIterReached =4
