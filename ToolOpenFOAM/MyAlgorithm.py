'''
@author: michal
'''

from ToolBase.constants import allowedSolverStatus
import ToolBase.AlgorithmBase

class MyAlgorithm(ToolBase.AlgorithmBase.AlgorithmBase):
    '''
    Custom execution algorithm
    '''

    def __init__(self,job):
        self.job = job
        self.Cd  = list()
        self.levelMin = 0
        self.levelMax = 2


    def run(self):
        '''
        Runs the algorithm
        '''
        self.job.preprocessGeometry()

        # initialize loop control variables
        convergedOnGrids=False
        refineGrid=False

        # refinement loop
        loopCounter = 0
        while (not convergedOnGrids):
            loopCounter+=1
            print("")
            print("***** Run #",loopCounter,"starts")
            # grid will not be refined in the first iteration
            if refineGrid:
                self.levelMin+=1
                self.levelMax+=1

            self.job.refineMesh(param={
                "levelMin":self.levelMin,
                "levelMax":self.levelMax,
                })

            self.job.clearResults()

            # mesh creation and checking is assumed to always succeed
            # handling issues related to snappyMesh failing or producing
            # low quality mesh is a complex issue in itself
            self.job.preprocessMesh()
            self.job.checkMesh()

            # run solver
            status = self.job.runSolver()
            print("***** Run #",loopCounter,"status ",status.name)

            # decisions
            if status==allowedSolverStatus.converged:
                # now check if converged on grids
                convergedOnGrids = self.checkConvergenceOnGrids()
                refineGrid = not convergedOnGrids
            else:
                # when stalled, failed, or reach end time
                # repeat on finer grid
                refineGrid=True


    def checkConvergenceOnGrids(self):
        '''
        True if Cd converges on the grid to predefined threshold or
        maximum number of refinement levels reached
        '''
        def gridConvergenceCriterion2(lst,threshold=0.10):
            '''
            True if 2 elements of the lst are within narrow threshold
            '''
            if len(lst)<2: return False
            return ((abs(lst[-1]-lst[-2])/max(lst[-1],lst[-2])) < threshold)

        def gridConvergenceCriterion3(lst,threshold=0.20):
            '''
            True if 3 elements of the lst are within wide threshold
            '''
            if len(lst)<3: return False
            return (((abs(lst[-1]-lst[-2])/max(lst[-1],lst[-2])) < threshold) and
                    ((abs(lst[-1]-lst[-3])/max(lst[-1],lst[-3])) < threshold))

        # save analyzed data
        self.Cd.append(self.job.runner.getAnalyzer("Forces").getTimeline("Cd")[1][-1])
        print()
        print("Cd values on previous runs:\n",self.Cd)

        if self.levelMax >= self.job.maxLevels:
            convergedOnGrids = True
            print("Solution converged on grids. y+==1 resolution reached.")
        else:
            convergedOnGrids = False
            if ( gridConvergenceCriterion2(self.Cd) or gridConvergenceCriterion3(self.Cd) ):
                convergedOnGrids = True
                print("Solution converged on grids. Grid convergence criterion matched")

        return convergedOnGrids
