'''
@author: michal
'''
import re, sys

from PyFoam.LogAnalysis.GeneralLineAnalyzer import GeneralLineAnalyzer
from PyFoam.LogAnalysis.StandardLogAnalyzer import StandardLogAnalyzer

class ForcesLineAnalyzer(GeneralLineAnalyzer):

    def __init__(self,
                 doTimelines=True,
                 doFiles=False,
                 singleFile=False,
                 startTime=None,
                 endTime=None):
        GeneralLineAnalyzer.__init__(self,
                                     titles=[],
                                     doTimelines=doTimelines,
                                     doFiles=doFiles,
                                     singleFile=singleFile,
                                     startTime=startTime,
                                     endTime=endTime)

        self.exp=re.compile("^(C.+)    = (.+)$")

        if self.doTimelines:
            self.lines.setDefault(1.)
            self.lines.setExtend(True)

    def addToTimelines(self,match):
        name=self.fName(match.groups()[0])
        value=match.groups()[1]
        self.lines.setValue(name,value)


class SolutionConvergedLineAnalyzer(GeneralLineAnalyzer):

    def __init__(self,
                 doTimelines=True,
                 doFiles=False,
                 singleFile=False,
                 startTime=None,
                 endTime=None):
        GeneralLineAnalyzer.__init__(self,
                                     titles=["Local"],
                                     doTimelines=doTimelines,
                                     doFiles=doFiles,
                                     singleFile=singleFile,
                                     startTime=startTime,
                                     endTime=endTime)
        self.told=""
        self.exp=re.compile("^(.+) solution converged in (.+) iterations$")

    def addToTimelines(self,match):
        self.lines.setAccumulator("converged","last")
        self.lines.setValue("converged",1)
        print("Converged!")
        print(self.lines)
        raise("error")


class OpenFOAMLogAnalyzer(StandardLogAnalyzer):
    '''
    Analyzes log using several line analyzers
    '''

    def __init__(self):
        StandardLogAnalyzer.__init__(self,progress=True,doTimelines=True,doFiles=False)
        self.addAnalyzer("Forces",ForcesLineAnalyzer())
        self.addAnalyzer("Convergence", SolutionConvergedLineAnalyzer())

