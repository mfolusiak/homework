'''
@author: michal
'''

import os
import numpy as np

from ToolBase.constants import allowedSolverStatus

from PyFoam.Execution.StepAnalyzedRunner import StepAnalyzedRunner

class ConvergenceCheckingRunner(StepAnalyzedRunner):

    # parameters
    linearSolverRelTolThreshold = 1e-4


    def __init__(self,
                 analyzer,
                 argv=None,
                 silent=False,
                 logname="PyFoamSolve",
                 smallestFreq=0.,
                 server=False,
                 remark=None,
                 parameters=None,
                 jobId=None,
                 echoCommandLine=None):
        StepAnalyzedRunner.__init__(self,
                                    analyzer,
                                    argv,
                                    silent,
                                    logname,
                                    smallestFreq,
                                    server,
                                    remark,
                                    parameters,
                                    jobId,
                                    echoCommandLine)

        # register function ran every iteration
        #
        self.status = allowedSolverStatus.maxIterReached
        self.addTicker(self.checkConvergence)

    def checkConvergence(self):
        """
        Function ran every iteration. Implements convergence checking criteria
        based on Cx and check if residuals not stalled

        Note that the tolerances in fvSolutin are set to very small
        values in the data/basecase/ to avoid OpenFOAM stopping before convergence
        """

        currentLinear=self.getAnalyzer("Linear").getCurrentData()

        """
        converged=True
        print(currentLinear)
        if 'p_final' in currentLinear.keys():
            if currentLinear['p_final'] > self.linearSolverRelTolThreshold: converged=False
        else:
            converged=False
        if 'Ux_final' in currentLinear.keys():
            if currentLinear['Ux_final'] > self.linearSolverRelTolThreshold: converged=False
        if 'Uy_final' in currentLinear.keys():
            if currentLinear['Uy_final'] > self.linearSolverRelTolThreshold: converged=False
        if 'k_final' in currentLinear.keys():
            if currentLinear['k_final'] > self.linearSolverRelTolThreshold: converged=False
        if 'omega_final' in currentLinear.keys():
            if currentLinear['omega_final'] > self.linearSolverRelTolThreshold: converged=False
        """

        converged = checkConvergedCx(self.getAnalyzer("Forces").getTimeline('Cd')[1])

        if converged:
            print("Solution has converged! Stopping solver.")
            self.status = allowedSolverStatus.converged
            self.stopAtNextWrite()

        if 'p' in currentLinear.keys():
            if checkStalledResiduals(self.getAnalyzer("Linear").getTimeline('p')[1]):
                print("Solution has stalled! Stopping solver.")
                self.status = allowedSolverStatus.stalled
                self.stopAtNextWrite()

    def stopGracefully(self):
        '''
        Tells the runner to stop at the next convenient time
        Overrides base method with more robust method of updating controlDict
        '''
        print("Stopping run and writing")
        shutil.copyfile(os.path.join(self.dir,"system","controlDictStop"),
                        os.path.join(self.dir,"system","controlDict"))

def checkConvergedCx(lst):
    '''
    check is the variance of Cd over last `window` iterations is within threshold
    '''
    window    = 20
    threshold = 0.02
    if len(lst)>window :
        variance = np.var(lst[:-window])
        mean     = np.mean(lst[:-window])
        #print(mean,variance,variance/mean)
        if (variance/mean)<threshold:
            return True
    return False

def checkStalledResiduals(lst):
    '''
    Function which can decide if simulations stalled
    Simulations are stalled if the slope of the two points on the running
    average of previous `window` data over the `past` range is positive
    for both short and long distance
    one could check variance here as well
    '''
    window    = 50
    pastShort = 25
    pastLong  = 50
    if len(lst)>window+pastLong:
        slopeShort = runningAverage(lst[:-pastShort],window)/runningAverage(lst,window)
        slopeLong  = runningAverage(lst[:-pastLong],window)/runningAverage(lst,window)
        if slopeShort < 1 and slopeLong < 1:
            return True
    return False

'''
running average implementation
'''
def runningAverage(x, window):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[window:] - cumsum[:-window]) / float(window)
