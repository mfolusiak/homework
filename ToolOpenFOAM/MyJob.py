'''
@author: michal
'''

import ToolBase.JobBase
from ToolBase.constants import allowedSolverStatus
import ToolOpenFOAM.SolverLogAnalyzers
import ToolOpenFOAM.SolverRunners

import os, glob, shutil, math
import numpy as np

from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile

surfaceFeatureExtractUtility = "surfaceFeatureExtract"
blockMeshUtility = "blockMesh"
snappyHexMeshUtility = "snappyHexMesh"
createPatchUtility = "createPatch"
extrudeMeshUtility = "extrudeMesh"
baseCase="data/basecase/"
geometryRelPath="constant/triSurface"

class MyJob(ToolBase.JobBase.JobBase):

    def __init__(self,srcBaseDir,workDir,case,solver,verbose=False):
        super().__init__(srcBaseDir,workDir,case,solver,verbose)
        # override default grid file location
        self._geometryFileCopyTo = os.path.join(geometryRelPath,self._geometryFileCopyTo)
        self.clearResults()


    def preprocessGeometry(self):
        '''
        override for Job preprocessing from ToolBase class
        '''
        # extract surface features
        extractSurfFeaRun = BasicRunner(argv=[surfaceFeatureExtractUtility,"-case",self._caseFullPath],
                                        server=False,silent=not self._verbose)
        print("Running "+surfaceFeatureExtractUtility)
        try:
            extractSurfFeaRun.start()
            if not extractSurfFeaRun.runOK():
                raise Exception("There was a problem running "+surfaceFeatureExtractUtility)
        except Exception as exc:
            print("Error: ",exc," Exiting...")
            raise SystemExit


    def preprocessMesh(self):
        # blockMesh
        blockMeshRun = BasicRunner(argv=[blockMeshUtility,"-case",self._caseFullPath],
                                   server=False,silent=not self._verbose)
        print("Running "+blockMeshUtility)
        try:
            blockMeshRun.start()
            if not blockMeshRun.runOK():
                raise Exception("There was a problem running "+blockMeshUtility)
        except Exception as exc:
            print("Error: ",exc," Exiting...")
            raise SystemExit

        # snappyHexMeshUtility -overwrite
        snappyHexMeshRun = BasicRunner(argv=[snappyHexMeshUtility,"-case",self._caseFullPath,"-overwrite"],
                                   server=False,silent=not self._verbose)
        print("Running "+snappyHexMeshUtility)
        try:
            snappyHexMeshRun.start()
            if not snappyHexMeshRun.runOK():
                raise Exception("There was a problem running "+snappyHexMeshUtility)
        except Exception as exc:
            print("Error: ",exc," Exiting...")
            raise SystemExit

        # createPatch -overwrite
        createPatchRun = BasicRunner(argv=[createPatchUtility,"-case",self._caseFullPath,"-overwrite"],
                                   server=False,silent=not self._verbose)
        print("Running "+createPatchUtility)
        try:
            createPatchRun.start()
            if not createPatchRun.runOK():
                raise Exception("There was a problem running "+createPatchUtility)
        except Exception as exc:
            print("Error: ",exc," Exiting...")
            raise SystemExit

        # extrudeMeshRun
        extrudeMeshRun = BasicRunner(argv=[extrudeMeshUtility,"-case",self._caseFullPath],
                                   server=False,silent=not self._verbose)
        print("Running "+extrudeMeshUtility)
        try:
            extrudeMeshRun.start()
            if not extrudeMeshRun.runOK():
                raise Exception("There was a problem running "+extrudeMeshUtility)
        except Exception as exc:
            print("Error: ",exc," Exiting...")
            raise SystemExit


    def refineMesh(self,param):
        '''
        Modifies snappy mesh dictionary
        @returns current state of modified variabled
        @todo param keys could be translated into list of keys used to access snappyHexMeshDict
        '''
        snappyHexMeshDict=ParsedParameterFile(os.path.join(self._caseFullPath,"system/snappyHexMeshDict"))
        snappyHexMeshDict["castellatedMeshControls"]["refinementSurfaces"]["geometry"]["level"][0]=param["levelMin"]
        print("Geometry level min in system/snappyHexMeshDict ",param["levelMin"])
        snappyHexMeshDict["castellatedMeshControls"]["refinementSurfaces"]["geometry"]["level"][1]=param["levelMax"]
        print("Geometry level max in system/snappyHexMeshDict ",param["levelMax"])
        _writeDictFile(snappyHexMeshDict)


    def runSolver(self):
        '''
        Runs OpenFOAM solver
        '''
        logAnalyzer = ToolOpenFOAM.SolverLogAnalyzers.OpenFOAMLogAnalyzer()
        self.runner = ToolOpenFOAM.SolverRunners.ConvergenceCheckingRunner(
                        argv=[self._solver,"-case",self._caseFullPath],
                        analyzer=logAnalyzer,
                        silent=not self._verbose)
        self.runner.start()

        # reset control file
        print("Reset control file")
        self.resetControlDict()

        # return status to algorithm
        if self.runner.runOK():
            return self.runner.status
        else:
            return allowedSolverStatus.failed


    def resetControlDict(self):
        """Tells the runner to stop at the next convenient time"""
        print("Stopping run and writing")
        shutil.copyfile(os.path.join(self._srcBaseDir,baseCase,"system","controlDict"),
                        os.path.join(self._caseFullPath,"system","controlDict"))


    def createCase(self):
        '''
        Copy sample case from source and chdir there
        '''
        # modify dictionary here
        orig=SolutionDirectory(name=os.path.join(self._srcBaseDir,baseCase),
                               archive=None,
                               paraviewLink=False)

        orig.cloneCase(self._caseFullPath)
        os.chdir(self._caseFullPath)


    def setupCase(self,param):
        '''
        modify dictionary here
        '''
        # get parameters
        velocity = param["velocity"]
        nDim = param["nDim"]
        nCellsDim = param["nCellsDim"]
        rho = param["propertiesAir"]["rho"]
        nu  = param["propertiesAir"]["nu"]
        farfieldFactor=param["farfieldFactor"]
        # other constants
        firstLayerThickness = 0.2

        # get geometry dimensions
        geometryMin = np.array(self._geometryBB[0])
        geometryMax = np.array(self._geometryBB[1])
        geometryBBSize = geometryMax-geometryMin
        geometryCenter = (geometryMax+geometryMin)/2
        refLength=round(float(max(geometryBBSize[0:nDim-1])),4)
        baseMeshMin = geometryCenter-farfieldFactor*np.array([refLength,refLength,refLength])
        baseMeshMin = baseMeshMin.tolist()
        baseMeshMin = [ round(x,4) for x in baseMeshMin ]
        baseMeshMax = geometryCenter+farfieldFactor*np.array([refLength,refLength,refLength])
        baseMeshMax = baseMeshMax.tolist()
        baseMeshMax = [ round(x,4) for x in baseMeshMax ]
        baseMeshMeanCellSize = 2*farfieldFactor*refLength/nCellsDim
        print("Geometry:")
        print(" - geometryBBSize       : ",geometryBBSize)
        print(" - geometryCenter       : ",geometryCenter)

        # derived variables - cf. http://www.pointwise.com/yplus/
        Re = velocity*refLength/nu
        wallSpacingForYPlus1 = nu*rho/velocity/math.sqrt(0.013/Re**(1/7))
        # minCellSize is size of the cell on the highest level based on wallSpacingForYPlus1
        minCellSize = wallSpacingForYPlus1/firstLayerThickness
        # set max levels for the job
        self.maxLevels = math.ceil(math.log(baseMeshMeanCellSize/minCellSize,2))


        print("Setting up case file")

        changeDictionaryDict=ParsedParameterFile(os.path.join(self._caseFullPath,"system/changeDictionaryDict"))
        assert changeDictionaryDict["U"]["boundaryField"]["boundaries"]["freestreamValue"]["value"][0] == 40
        changeDictionaryDict["U"]["boundaryField"]["boundaries"]["freestreamValue"]["value"][0]=velocity
        _writeDictFile(changeDictionaryDict)

        controlDict=ParsedParameterFile(os.path.join(self._caseFullPath,"system/controlDict"))
        assert controlDict["functions"]["forces"]["magUInf"] == 40
        controlDict["functions"]["forces"]["magUInf"]=velocity
        assert controlDict["functions"]["forces"]["Aref"] == 1.0
        controlDict["functions"]["forces"]["Aref"]=refLength
        assert controlDict["functions"]["forces"]["lRef"] == 1.0
        controlDict["functions"]["forces"]["lRef"]=refLength
        assert controlDict["functions"]["forces"]["rhoInf"] == 1.0
        controlDict["functions"]["forces"]["rhoInf"]=rho
        _writeDictFile(controlDict)

        U0=ParsedParameterFile(os.path.join(self._caseFullPath,"0/U"))
        assert U0["boundaryField"]["boundaries"]["freestreamValue"]["value"][0] == 40
        U0["boundaryField"]["boundaries"]["freestreamValue"]["value"][0]=velocity
        _writeDictFile(U0)

        blockMeshDict=ParsedParameterFile(os.path.join(self._caseFullPath,"system/blockMeshDict"))
        assert blockMeshDict["vertices"][0][0] == -15.0
        _writeDictFile(blockMeshDict)

        snappyHexMeshDict=ParsedParameterFile(os.path.join(self._caseFullPath,"system/snappyHexMeshDict"))
        assert snappyHexMeshDict["addLayersControls"]["layers"]["geometry"]["firstLayerThickness"] == firstLayerThickness
        assert snappyHexMeshDict["addLayersControls"]["layers"]["geometry_patch0"]["firstLayerThickness"] == firstLayerThickness
        assert snappyHexMeshDict["addLayersControls"]["firstLayerThickness"] == firstLayerThickness
        _writeDictFile(snappyHexMeshDict)

        print(" - free-stream velocity set to          : ",velocity)
        print(" - reference length set to              : ",refLength)
        print(" - Re                                   : ",Re)
        print(" - calculated wall spacing for y+==1    : ",wallSpacingForYPlus1)
        print(" - base mesh extents                    : ",baseMeshMin)
        print("                                        : ",baseMeshMax)
        print(" - base mesh mean cell size             : ",baseMeshMeanCellSize)
        print(" - min mesh size allowed to reach y+==1 : ",minCellSize)
        print(" - levels allowed to reach y+==1        : ",self.maxLevels)

    def checkMesh(self):
        '''
        Placeholder for checkMesh call
        '''
        return True

    def clearResults(self):
        try:
            solDir=SolutionDirectory(self._caseFullPath)
            solDir.clearResults()
            solDir.clearOther(pyfoam=True, removeAnalyzed=True, verbose=False)
            try:
                os.remove(os.path.join(self._caseFullPath,self._case+".foam"))
            except FileNotFoundError:
                pass

            shutil.rmtree(os.path.join(self._caseFullPath,"constant/polyMesh"),ignore_errors=True)
            shutil.rmtree(os.path.join(self._caseFullPath,"constant/extendedFeatureEdgeMesh"),ignore_errors=True)

            for f in glob.glob(os.path.join(self._caseFullPath,"PyFoam*")):
                if os.path.isdir(f):
                    shutil.rmtree(f)
                else:
                    os.remove(f)
        except FileNotFoundError:
            # pass if the directory doesn't exist yet
            pass

# helper function
def _writeDictFile(f):
    try:
        f.writeFile()
    except IOError:
        print("Can't write file. Content would have been:")
        print(f)
        raise