'''
@author: michal
Provide either both geometryFile and velocity as arguments or none for default input
'''

import os, sys, shutil

# const
solver="simpleFoam"
case="wind_tunnel"
farfieldFactor=10.0 # times the refLength = distance from geometry center to the boundary
nCellsDim = 100 # number of cells per dimension on base mesh
nDim=2 # dimensionality
propertiesAir = {"rho":1.225,"nu":1.5e-5}


def main(geometryFile,velocity,JobClass,AlgorithmClass):
    # validate input
    if not os.path.isfile(geometryFile):
        raise (FileNotFoundError("Provided geometry file not found"))
    try:
        velocity = float(velocity)
    except ValueError:
        raise (ValueError("Provided Velocity is not of type Float"))

    # setup
    srcBaseDir = os.path.dirname(os.path.abspath(__file__))
    workDir = os.getcwd()
    job = JobClass(srcBaseDir=srcBaseDir,workDir=workDir,case=case,solver=solver,
                   verbose=False)

    # remove case from workDir/
    shutil.rmtree(job._caseFullPath,ignore_errors=True)

    # copy sample case from srcBaseDir/data to workDir/
    job.createCase()

    job.loadGeometryFile(os.path.join(srcBaseDir,geometryFile))

    job.setupCase({
        "velocity":velocity,
        "nDim":nDim,
        "nCellsDim":nCellsDim,
        "propertiesAir":propertiesAir,
        "farfieldFactor":farfieldFactor,
        })

    myAlgorithm = AlgorithmClass(job)
    myAlgorithm.run()

import ToolOpenFOAM.MyJob
import ToolOpenFOAM.MyAlgorithm

if __name__ == '__main__':
    if len(sys.argv) == 1:
        geometryFile = os.path.join(os.path.dirname(os.path.abspath(__file__)),"data/wing.stl")
        velocity = 20.0
    else:
        if len(sys.argv) == 3:
            geometryFile = os.path.abspath(sys.argv[1])
            velocity = sys.argv[2]
        else:
            raise(ValueError('Provide either both geometryFile and velocity as arguments or none for default input'))

    main(geometryFile = geometryFile,
         velocity = velocity,
         JobClass=ToolOpenFOAM.MyJob.MyJob,
         AlgorithmClass=ToolOpenFOAM.MyAlgorithm.MyAlgorithm)
