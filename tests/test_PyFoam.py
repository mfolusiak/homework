import unittest, os

import PyFoam
import PyFoam.FoamInformation
import PyFoam.RunDictionary.ParsedParameterFile

class TestPyFoam(unittest.TestCase):

    def test_OpenFOAM4tutorialsFound(self):
        self.assertEqual(PyFoam.FoamInformation.foamTutorials(),"/opt/openfoam4/tutorials")

    def test_CanModifySnappyHexMeshDict(self):
        srcBaseDir = os.path.dirname(os.path.abspath(__file__))
        file=PyFoam.RunDictionary.ParsedParameterFile.ParsedParameterFile(os.path.join(srcBaseDir,"../data/basecase/system/snappyHexMeshDict"))
        self.assertEqual(file["geometry"]["geometry.stl"]["includedAngle"],120.0)
        self.assertEqual(file["castellatedMeshControls"]["refinementSurfaces"]["geometry"]["level"],[0,2])
        file["castellatedMeshControls"]["refinementSurfaces"]["geometry"]["level"]=[2,5]
        self.assertEqual(file["castellatedMeshControls"]["refinementSurfaces"]["geometry"]["level"],[2,5])

if __name__ == '__main__':
    unittest.main()