# specify base image
FROM ubuntu:16.04
MAINTAINER "Michal Folusiak" <michal.folusiak@gmail.com>

# Add OpenFOAM repo
RUN  echo deb http://dl.openfoam.org/ubuntu xenial main >> /etc/apt/sources.list.d/openfoam.list
RUN apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y --force-yes apt-transport-https

# Install OpenFOAM
RUN \
  apt-get update && \
  apt-get install -y --force-yes openfoam4

# Copy dir content and cwd
RUN mkdir -p /usr/src/app
COPY . /usr/src/app/
WORKDIR /usr/src/app

# Install python
RUN \
  apt-get update && \
  apt-get install -y python3 python3-pip python3-dev && \
  pip3 install --upgrade pip && \
  pip3 install --no-cache-dir -r requirements.txt

# run the command
CMD . /opt/openfoam4/etc/bashrc && python3 -m unittest discover tests/ && \
                                   python3 ./main.py
